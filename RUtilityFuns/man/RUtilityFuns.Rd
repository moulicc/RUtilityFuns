\name{Utility Functions}
\alias{Utility Functions}
%- Also NEED an '\alias' for EACH other topic documented here.
\title{
  Utility Functions
}
\description{
  Imputation Functions to start with and will be adding more.

  Prints summary of pct of NA values for each column and the corresponding value used for imputation to console

  The returned list will have a dataframe of imputed columns and an array of imputed values to use in future on a scoring data set using 'Impute_4_scoring_R' function!
}
\usage{
impute_R(df,choice=2,.)
Impute_4_scoring_R(df,imp_values,.)
}
%- maybe also 'usage' for other objects documented here.
\arguments{
  \item{df Input dataset as a dataframe}
  \item{choice 1= mean & 2 = median for numeric columns||Mode for categorical columns by default}
  \item{imp_values Input array of constant imputed values from "impute_r" function (Applicable only for 'Impute_4_scoring_R' function)}
  {
%%     ~~Describe \code{x} here~~
}
}
\details{
%%  ~~ If necessary, more details than the description above ~~
}
\value{
%%  ~Describe the value returned
%%  If it is a LIST, use
 \item{imp_df }{Data frame of imputed columns with column names renamed as 'Imp_'&original column name}
 \item{imp_values }{Array of imputed values for each column; 'No' for column where no imputation is done!}
%% ...
}
\references{
%% ~put references to the literature/web site here ~
}
\author{
  ~~Chirangeevi Cherukuri~~
}
\note{
%%  ~~further notes~~
}

%% ~Make other sections like Warning with \section{Warning }{....} ~

\seealso{
%% ~~objects to See Also as \code{\link{help}}, ~~~
}
\examples{
##train<-read.csv("https://docs.google.com/spreadsheets/d/1jlTaQR1gVOXzTZSfpvN136CeHGfVgyNeWDBAai43YGA/pub?gid=1767763027&single=true&output=csv",na = c("", NA))

##imp_info_list<-impute_R(train)

##impute_4_scoring_R(train,(imp_info_list$imp_values))

%%## The function is currently defined as
%%function (x)
%%{
%%  }
%%}
% Add one or more standard keywords, see file 'KEYWORDS' in the
% R documentation directory.

%\keyword{ ~Imputation }% use one of  RShowDoc("KEYWORDS")
%\keyword{ ~Scoring}% __ONLY ONE__ keyword per line
